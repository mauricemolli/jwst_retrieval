import numpy as np
import rebin_give_width as rgw
from scipy.ndimage.filters import gaussian_filter

class Data:

    def __init__(self, \
                 name, \
                 path_to_observations = None, \
                 data_resolution = None, \
                 wlen_bins = None, \
                 photometry = False, \
                 photometric_transfomation_function = None, \
                 photometry_range = None, \
                 external_pRT_reference = None, \
                 model_generating_function = None, \
                 generate_spectrum_wlen_range_micron = None, \
                 width_photometry = None):

        self.name = name
        self.path_to_observations = path_to_observations
        self.data_resolution = data_resolution
        self.external_pRT_reference = external_pRT_reference
        self.model_generating_function = model_generating_function
        self.generate_spectrum_wlen_range_micron = \
            generate_spectrum_wlen_range_micron
        self.photometry = photometry
        self.photometric_transfomation_function = \
            photometric_transfomation_function
        self.photometry_range = photometry_range
        self.width_photometry = width_photometry
        
        # Read in data
        if path_to_observations != None:
            obs = np.genfromtxt(path_to_observations)
            if not self.photometry:
                self.wlen = obs[:,0]
                self.flux = obs[:,1]
                self.flux_error = obs[:,2]
                self.wlen_range_pRT = [0.95 * self.wlen[0], \
                                       1.05 * self.wlen[-1]]
            else:
                self.wlen = obs[0]
                self.flux = obs[1]
                self.flux_error = obs[2]
                self.wlen_range_pRT = [self.photometry_range[0], \
                                       self.photometry_range[1]]
                
            # For binning later
            if not self.photometry:
                if wlen_bins != None:
                    self.wlen_bins = wlen_bins
                else:
                    self.wlen_bins = np.zeros_like(self.wlen)
                    self.wlen_bins[:-1] = np.diff(self.wlen)
                    self.wlen_bins[-1] = self.wlen_bins[-2]
        else:
            self.wlen_range_pRT = \
                self.generate_spectrum_wlen_range_micron

        # To be filled later
        self.pRT_object = None


    def get_chisq(self, wlen_model, \
                  spectrum_model, \
                  plotting):

        if plotting:
            import pylab as plt

        if not self.photometry:
            # Convolve to data resolution
            if self.data_resolution != None:
                spectrum_model = self.convolve(wlen_model, \
                            spectrum_model, \
                            self.data_resolution)

            # Rebin to model observation
            flux_rebinned = rgw.rebin_give_width(wlen_model, \
                                                 spectrum_model, \
                                                 self.wlen, \
                                                 self.wlen_bins)
        else:
            flux_rebinned = \
                self.photometric_transfomation_function(wlen_model, \
                                                  spectrum_model)
            
        diff = (flux_rebinned - self.flux)
        logL = -np.sum( (diff / self.flux_error)**2. ) / 2.

        if plotting:
            plt.plot(wlen_model, spectrum_model)
            if not self.photometry:
                plt.plot(self.wlen, flux_rebinned)
                plt.errorbar(self.wlen, \
                             self.flux, \
                             yerr = self.flux_error, \
                             fmt = '+')
            else:
                plt.errorbar(self.wlen, flux_rebinned, \
                             xerr = self.width_photometry/2.)
                plt.errorbar(self.wlen, \
                             self.flux, \
                             yerr = self.flux_error, \
                             xerr = self.width_photometry/2., \
                             fmt = '+')
            plt.show()
        
        return logL

    def convolve(self, \
                 input_wavelength, \
                 input_flux, \
                 instrument_res):
    
        # From talking to Ignas: delta lambda of resolution element
        # is FWHM of the LSF's standard deviation, hence:
        sigma_LSF = 1./instrument_res/(2.*np.sqrt(2.*np.log(2.)))

        # The input spacing of petitRADTRANS is 1e3, but just compute
        # it to be sure, or more versatile in the future.
        # Also, we have a log-spaced grid, so the spacing is constant
        # as a function of wavelength
        spacing = np.mean(2.*np.diff(input_wavelength)/ \
                          (input_wavelength[1:]+input_wavelength[:-1]))

        # Calculate the sigma to be used in the gauss filter in units
        # of input wavelength bins
        sigma_LSF_gauss_filter = sigma_LSF/spacing
    
        flux_LSF = gaussian_filter(input_flux, \
                                   sigma = sigma_LSF_gauss_filter, \
                                   mode = 'nearest')

        return flux_LSF
