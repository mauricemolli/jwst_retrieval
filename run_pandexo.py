import warnings
warnings.filterwarnings('ignore')
import pandexo.engine.justdoit as jdi # THIS IS THE HOLY GRAIL OF PANDEXO
import pandexo.engine.justplotit as jpi

import numpy as np
import os
#pip install pandexo.engine --upgrade

import run_definition as rd
from parameter_class import *
#from data_class import *

#################################################
#################################################
###
### JWST
###
#################################################
#################################################

exo_dict = jdi.load_exo_dict()
print(exo_dict.keys())
#print(exo_dict['star']['w_unit'])




exo_dict['observation']['sat_level'] = 80    #saturation level in percent of full well 
exo_dict['observation']['sat_unit'] = '%'
exo_dict['observation']['noccultations'] = 1 #number of transits 
exo_dict['observation']['R'] = None          #fixed binning. I usually suggest ZERO binning.. you can always bin later 
                                             #without having to redo the calcualtion
exo_dict['observation']['baseline_unit'] = 'frac'  #Defines how you specify out of transit observing time
                                                    #'frac' : fraction of time in transit versus out = in/out 
                                                    #'total' : total observing time (seconds)
exo_dict['observation']['baseline'] = 1. #in accordance with what was specified above (total observing time)


exo_dict['star']['type'] = 'phoenix'        #phoenix or user (if you have your own)
exo_dict['star']['mag'] = rd.parameters['Kmag_star'].value #magnitude of the system
exo_dict['star']['ref_wave'] = 2.22         #For J mag = 1.25, H = 1.6, K =2.22.. etc (all in micron)
exo_dict['star']['temp'] = rd.parameters['Teff_star'].value #in K 
exo_dict['star']['metal'] = rd.parameters['[Fe/H]_star'].value # as log Fe/H
exo_dict['star']['logg'] = rd.parameters['logg_star'].value #log surface gravity cgs



exo_dict['planet']['type'] ='user'                       #tells pandexo you are uploading your own spectrum
exo_dict['planet']['exopath'] = 'generated_spectra/'+rd.pandexo_name+'.dat'
exo_dict['planet']['w_unit'] = rd.parameters['wlen_unit_pandexo'].value   #other options include "um","nm" ,"Angs", "sec" (for phase curves)
exo_dict['planet']['f_unit'] = rd.parameters['flux_definition_pandexo'].value   #other options are 'fp/f*' 
exo_dict['planet']['transit_duration'] = rd.parameters['transit_duration_in_seconds'].value  #transit duration 
exo_dict['planet']['td_unit'] = 's'                      #Any unit of time in accordance with astropy.units can be added

jdi.print_instruments()

results = {}

exo_dict['observation']['noise_floor'] = 75.
results['NIRSpec G395M'] = jdi.run_pandexo(exo_dict,['NIRSpec G395M'])

exo_dict['observation']['noise_floor'] = 40.
results['MIRI LRS'] = jdi.run_pandexo(exo_dict,['MIRI LRS'])

exo_dict['observation']['noise_floor'] = 20.
results['NIRISS SOSS'] = jdi.run_pandexo(exo_dict,['NIRISS SOSS'])

spectra = {}
for name in results.keys():
    x,y,e = jpi.jwst_1d_spec(results[name], model=False, x_range=[0.5,20])
    x = np.array(x).reshape(np.shape(x)[1],1)
    y = np.array(y).reshape(np.shape(y)[1],1)
    e = np.array(e).reshape(np.shape(e)[1],1)
    spectra[name] = [x,y,e]
    np.savetxt('observations/'+name.replace(' ','_')+'.dat', \
               np.column_stack((np.array(spectra[name][0]), \
                                np.array(spectra[name][1]), \
                                np.array(spectra[name][2]))), \
               header = 'Wlen (micron), (Rpl/Rs)^2, (Rpl/Rs)^2 error')
    print()
    print()

