###########################################
# Input / output, general run definitions
###########################################

# Read external packages
import numpy as np
import pymultinest
import json
import os
# To not have numpy start parallelizing on its own
os.environ["OMP_NUM_THREADS"] = "1"

# Read own packages
from petitRADTRANS import Radtrans
from petitRADTRANS import nat_cst as nc
from parameter_class import *
from data_class import *
import run_definition as rd

###########################################
# Run-type setup
###########################################

# Run retrieval or generate synthetic spectrum?
run_mode = rd.run_mode

if run_mode == 'evaluate':
    best_fit_specs = {}
if run_mode == 'spectrum':
    generated_spectra = {}
    
# Plots for testing?
plotting = rd.plotting

# If retrieval mode, but also initially
# for evaluate mode!
PT_plot_mode = False
    
###########################################
# Observations
###########################################

data = rd.data

###########################################
# Create pRT radiative transfer objects
###########################################

for dd in data:
    # Only create if there's no other data
    # object using the same pRT object
    if dd.external_pRT_reference == None:
        rt_object = Radtrans(line_species = rd.line_species, \
                    rayleigh_species= rd.rayleigh_species, \
                    continuum_opacities = rd.continuum_opacities, \
                    cloud_species = rd.cloud_species, \
                    mode='c-k', \
                    wlen_bords_micron = dd.wlen_range_pRT)

        # Create random P-T profile to create RT arrays of the Radtrans object.
        temp_params = {}
        temp_params['log_delta'] = -6.
        temp_params['log_gamma'] = np.log10(0.4)
        temp_params['t_int'] = 750.
        temp_params['t_equ'] = 0.
        temp_params['log_p_trans'] = -3.
        temp_params['alpha'] = 0.
        p, t = nc.make_press_temp(temp_params)
        rt_object.setup_opa_structure(p)
        
        dd.pRT_object = rt_object


######################################################
# Parameters for system, parameters to be retrieved
######################################################

parameters = rd.parameters
#print(parameters)

######################################################
# Define prior function
######################################################

def prior(cube, ndim, nparams):

    i_p = 0

    for pp in parameters:
        if parameters[pp].is_free_parameter:
            cube[i_p] = parameters[pp].get_param_uniform(cube[i_p])
            i_p += 1

######################################################
# Define log probability
######################################################

def loglike(cube, ndim, nparams):

    i_p = 0
    log_likelihood = 0.
    log_prior      = 0.

    for pp in parameters:
        if parameters[pp].is_free_parameter:
            parameters[pp].set_param(cube[i_p])
            i_p += 1

    for dd in data:
        # Only calculate spectra within a given
        # wlen range once
        if dd.external_pRT_reference == None:
            try:
                if not PT_plot_mode:
                    wlen_model, spectrum_model = \
                        dd.model_generating_function(dd.pRT_object, \
                                                     parameters, \
                                                     PT_plot_mode)
                else:
                    if dd.name == rd.take_PTs_from:
                        pressures, temperatures = \
                            dd.model_generating_function(dd.pRT_object, \
                                                         parameters, \
                                                         PT_plot_mode)
                        return pressures, temperatures
                        
            except:
                print('Something went wrong generating a spectrum / PT profile!')
                return -np.inf

            if run_mode == 'spectrum':
                generated_spectra[dd.name] = [wlen_model, \
                                           spectrum_model]
                continue
            
            log_likelihood += dd.get_chisq(wlen_model, \
                                           spectrum_model, \
                                           plotting)

            if run_mode == 'evaluate':
                np.savetxt('evaluate_data/model_spec_best_fit'+ \
                           dd.name+'.dat', \
                           np.column_stack((wlen_model, \
                                            spectrum_model)))
                best_fit_specs[dd.name] = [wlen_model, \
                                           spectrum_model]
            
            # Check for data using the same pRT object,
            # calculate log_likelihood
            for dede in data:
                if dede.external_pRT_reference != None:
                    if dede.external_pRT_reference == dd.name:
                        log_likelihood += dede.get_chisq(wlen_model, \
                                           spectrum_model, \
                                           plotting)

    if run_mode == 'spectrum':
        return

    print(log_likelihood + log_prior)
    return log_likelihood + log_prior

######################################################
# Run retrieval, or evaluate retrieval
######################################################

if run_mode == 'retrieval':

    # How many free parameters?
    n_params = 0
    free_parameter_names = []

    for pp in parameters:
        if parameters[pp].is_free_parameter:
            free_parameter_names.append(parameters[pp].name)
            n_params += 1

    pymultinest.run(loglike, \
                    prior, \
                    n_params, \
                    outputfiles_basename='out_PMN/'+rd.retrieval_name+'_', \
                    resume = True, \
                    verbose = True, \
                    const_efficiency_mode = False, \
                    n_live_points = 1000)

    json.dump(free_parameter_names, \
              open('out_PMN/'+rd.retrieval_name+'_params.json', 'w'))

    # Done.
    
elif run_mode == 'evaluate':

    import pylab as plt

    # Read in samples
    samples = np.genfromtxt('out_PMN/'+ \
                            rd.retrieval_name+ \
                            '_post_equal_weights.dat')

    parameters_read = json.load(open('out_PMN/'+ \
                                rd.retrieval_name+ \
                                '_params.json'))

    ###########################################
    # Plot best-fit spectrum
    ###########################################

    import copy as cp
    samples_use = cp.copy(samples)
    i_p = 0
    for pp in parameters:
        if parameters[pp].is_free_parameter:
            for i_s in range(len(parameters_read)):
                if parameters_read[i_s] == parameters[pp].name:
                    samples_use[:,i_p] = samples[:, i_s]
            i_p += 1
            
    # Get best-fit index
    logL = samples_use[:,-1]
    best_fit_index = np.argmax(logL)

    loglike(samples_use[best_fit_index, :-1], 0, 0)

    model_max = 0
    model_min = 9e99
    
    for dd in data:

        try:
            resolution_data = np.mean(dd.wlen[1:]/np.diff(dd.wlen))
            ratio = resolution_data / rd.resolution
            if int(ratio) > 1:
                wlen = nc.running_mean(dd.wlen, int(ratio))[::int(ratio)]
                error = nc.running_mean(dd.flux_error / int(np.sqrt(ratio)), \
                                        int(ratio))[::int(ratio)]
                flux = nc.running_mean(dd.flux, \
                                        int(ratio))[::int(ratio)]
            else:
                wlen = dd.wlen
                error = dd.flux_error
                flux = dd.flux
        except:
            wlen = dd.wlen
            error = dd.flux_error
            flux = dd.flux

        if not dd.photometry:
            plt.errorbar(wlen, \
                         flux * rd.y_axis_scaling, \
                         yerr = error * rd.y_axis_scaling, \
                         fmt = '.', \
                         label = dd.name, color = 'orange', zorder =10)
        else:
            plt.errorbar(wlen, \
                         flux * rd.y_axis_scaling, \
                         yerr = error * rd.y_axis_scaling, \
                         xerr = dd.width_photometry/2., \
                         fmt = '+', color = 'orange', zorder = 10, \
                         label = dd.name)

        if dd.external_pRT_reference == None:
            plt.plot(best_fit_specs[dd.name][0], \
                     best_fit_specs[dd.name][1] * rd.y_axis_scaling, \
                     color = 'black')
            model_max = max(model_max, \
                             np.max(best_fit_specs[dd.name][1] * \
                            rd.y_axis_scaling))
            model_min = min(model_min, \
                            np.min(best_fit_specs[dd.name][1] * \
                            rd.y_axis_scaling))
        else:
            plt.plot(best_fit_specs[dd.external_pRT_reference][0], \
                     best_fit_specs[dd.external_pRT_reference][1] * \
                     rd.y_axis_scaling, \
                     color = 'black', zorder = -10)

    plt.xscale(rd.xscale)
    plt.yscale(rd.yscale)
    plt.xlabel(rd.spec_xlabel)
    plt.ylabel(rd.spec_ylabel)
    plt.ylim([model_min*0.98, model_max*1.02])
    plt.legend(loc='best')
    plt.savefig('evaluate_data/best_fit_spec.pdf')
    plt.show()

    ###########################################
    # P-T envelope
    ###########################################

    plt.clf()
    # Now we wanna plot, turn PT_plot_mode
    PT_plot_mode = True

    import copy as cp
    samples_use = cp.copy(samples)
    i_p = 0
    for pp in parameters:
        if parameters[pp].is_free_parameter:
            for i_s in range(len(parameters_read)):
                if parameters_read[i_s] == parameters[pp].name:
                    samples_use[:,i_p] = samples[:, i_s]
            i_p += 1

    temps = []
    for i_s in range(len(samples_use)):
        pressures, t = loglike(samples_use[i_s, :-1], 0, 0)
        temps.append(t)

    temps = np.array(temps)
    temps_sort = np.sort(temps, axis=0)

    len_samp = len(samples_use)
    plt.fill_betweenx(pressures, \
                      x1 = temps_sort[0, :], \
                      x2 = temps_sort[-1, :], \
                      color = 'cyan', label = 'all')
    plt.fill_betweenx(pressures, \
                      x1 = temps_sort[int(len_samp*(0.5-0.997/2.)), :], \
                      x2 = temps_sort[int(len_samp*(0.5+0.997/2.)), :], \
                      color = 'brown', label = '3 sig')
    plt.fill_betweenx(pressures, \
                      x1 = temps_sort[int(len_samp*(0.5-0.95/2.)), :], \
                      x2 = temps_sort[int(len_samp*(0.5+0.95/2.)), :], \
                      color = 'orange', label = '2 sig')
    plt.fill_betweenx(pressures, \
                      x1 = temps_sort[int(len_samp*(0.5-0.68/2.)), :], \
                      x2 = temps_sort[int(len_samp*(0.5+0.68/2.)), :], \
                      color = 'red', label = '1 sig')

    plt.yscale('log')
    try:
        plt.ylim(rd.press_limits)
    except:
        plt.ylim([pressures[-1], pressures[0]])
    try:
        plt.xlim(rd.temp_limits)
    except:
        pass

    try:
        plt.plot(rd.input_T, rd.input_P, label = 'Input', color = 'black')
    except:
        pass
    
    plt.xlabel('T (K)')
    plt.ylabel('P (bar)')
    plt.legend(loc='best')
    plt.savefig('evaluate_data/PT_envelopes.pdf')
    plt.show()


    ###########################################
    # Corner plot
    ###########################################

    plt.clf()
    from nice_corner import nice_corner
    samples_use = cp.copy(samples)
    parameter_plot_indices = []
    parameter_ranges       = []
    parameters_use = cp.copy(parameters_read)

    i_p = 0
    for pp in parameters_read:
        parameter_ranges.append(parameters[pp].corner_ranges)
        if parameters[pp].plot_in_corner:
           parameter_plot_indices.append(i_p)
        if parameters[pp].corner_label != None:
            parameters_use[i_p] = parameters[pp].corner_label
        if  parameters[pp].corner_transform != None:
            samples_use[:, i_p] = \
                parameters[pp].corner_transform(samples_use[:, i_p])
        i_p += 1

    try:
        for mp in rd.made_up_params:
            parameter_ranges.append(rd.made_up_params[mp].corner_ranges)

            shape = np.shape(samples_use)

            receiving_array = np.zeros(shape[0] * \
                                       len(rd.made_up_params[mp].transform_parameters)).\
                                       reshape(shape[0], \
                                               len(rd.made_up_params[mp].transform_parameters))
            i_r = 0
            for name in rd.made_up_params[mp].transform_parameters:
                i_p = 0
                for pp in parameters_read:
                    if pp == name:
                         receiving_array[:,i_r] = samples_use[:, i_p]
                    i_p += 1
                i_r += 1

            values_append = rd.made_up_params[mp].transform_function(receiving_array)
            parameters_use.append(rd.made_up_params[mp].name)
            larger_samples = np.zeros(shape[0]*int(shape[1]+1)).reshape(shape[0],int(shape[1]+1))
            #print(np.shape(larger_samples), np.shape(samples_use))
            larger_samples[:,:-2] = samples_use[:,:-1]
            larger_samples[:,-2] = values_append
            larger_samples[:,-1] = samples_use[:,-1]
            samples_use = larger_samples
            parameter_plot_indices.append(int(shape[1]-1))
    except:
        pass

    output_file = 'evaluate_data/corner_nice.pdf'
    max_val_ratio = 5.

    #print(parameters_use, parameter_plot_indices, np.shape(samples_use))

    nice_corner(samples_use, \
                parameters_use, \
                output_file, \
                N_samples = len(samples_use), \
                parameter_plot_indices = parameter_plot_indices, \
                max_val_ratio = max_val_ratio, \
                parameter_ranges = parameter_ranges, \
                true_values = None)
    
elif run_mode == 'spectrum':

    import pylab as plt

    loglike(0, 0, 0)

    for dd in data:
        plt.savetxt('generated_spectra/'+dd.name+'.dat', \
                    np.column_stack((generated_spectra[dd.name][0], \
                                     generated_spectra[dd.name][1])))

        plt.clf()
        plt.plot(generated_spectra[dd.name][0], \
                 generated_spectra[dd.name][1] * \
                 rd.y_axis_scaling, \
                 label = dd.name)
        
        plt.xscale(rd.xscale)
        plt.yscale(rd.yscale)
        plt.xlabel(rd.spec_xlabel)
        plt.ylabel(rd.spec_ylabel)
        plt.savefig('generated_spectra/'+dd.name+'.pdf')
        plt.show()
