from data_class import *
from parameter_class import *
from petitRADTRANS import Radtrans
from petitRADTRANS import nat_cst as nc
from poor_mans_nonequ_chem_FeH_1e3 import poor_mans_nonequ_chem as pm

#######################################################
# Define run mode
#######################################################

#run_mode = 'evaluate' # evaluate run?
#run_mode = 'retrieval' # run retrieval?
#run_mode = 'spectrum' # make spectrum?

retrieval_name = 'WASP-32b'

# Only set True for debugging,
# when running the retrieval locally
plotting = False

# Only important when making input spectrum
generate_spectrum_wlen_range_micron = [0.6, 13.]

#######################################################
# Define parameters, uniform prior coordinate mappings
#######################################################

parameters = {}

########################
# Fixed parameters
########################

g = 1e1**3.77
parameters['gravity'] = Parameter('gravity', False, value = g)
parameters['Rstar'] = Parameter('Rstar', False, value = 1.11*nc.r_sun)

if run_mode == 'spectrum':
    parameters['Tint'] = Parameter('Tint', False, value = 200.)
    parameters['Tequ'] = Parameter('Tequ', False, value = 1560.)
    parameters['gamma'] = Parameter('gamma', False, value = 0.4)
    parameters['kappa_IR'] = Parameter('kappa_IR', False, value = 1e-2)
    parameters['R_pl'] = Parameter('R_pl', False, value = 1.18*nc.r_jup_mean)
    parameters['[Fe/H]'] = Parameter('[Fe/H]', False, value = 0.)
    parameters['C/O'] = Parameter('C/O', False, value = 0.55)

    # Parameters if using Pandexo
    parameters['Kmag_star'] = Parameter('Kmag_star', False, value = 10.16)
    parameters['Teff_star'] = Parameter('Teff_star', False, value = 6100.)
    parameters['[Fe/H]_star'] = Parameter('[Fe/H]_star', False, value = -0.13)
    parameters['logg_star'] = Parameter('logg_star', False, value = 4.39)
    parameters['wlen_unit_pandexo'] = Parameter('wlen_unit_pandexo', \
                                                False, value = 'um')
    parameters['flux_definition_pandexo'] = \
        Parameter('flux_definition_pandexo', False, value = 'rp^2/r*^2')
    parameters['transit_duration_in_seconds'] = \
        Parameter('transit_duration_in_seconds', False, value = 0.101 * \
                  24. * \
                  3600.)

########################
# Variable parameters
########################

if run_mode != 'spectrum':
    parameters['Tint'] = Parameter('Tint', True, \
                                  transform_prior_cube_coordinate = \
                                  lambda x : 1000.*x)
    parameters['Tequ'] = Parameter('Tequ', True, \
                                  transform_prior_cube_coordinate = \
                                  lambda x : 3000.*x)
    parameters['gamma'] = Parameter('gamma', True, \
                                  transform_prior_cube_coordinate = \
                                  lambda x : 2.*x)
    parameters['kappa_IR'] = Parameter('kappa_IR', True, \
                                  transform_prior_cube_coordinate = \
                                      lambda x : 1e1**(-5+8.*x))
    parameters['R_pl'] = Parameter('R_pl', True, \
                                  transform_prior_cube_coordinate = \
                                  lambda x : (0.5+2.5*x)*nc.r_jup_mean)
    parameters['[Fe/H]'] = Parameter('[Fe/H]', True, \
                                  transform_prior_cube_coordinate = \
                                  lambda x : -1.+2.*x)
    parameters['C/O'] = Parameter('C/O', True, \
                                  transform_prior_cube_coordinate = \
                                  lambda x : 0.1+1.5*x)

#######################################################
# Define species to be included as absorbers
#######################################################

line_species = ['CH4', 'H2O', 'CO2', 'HCN', 'CO', 'H2', 'H2S', 'NH3', 'C2H2', 'PH3', 'Na', 'K']
rayleigh_species = ['H2', 'He']
continuum_opacities = ['H2-H2', 'H2-He']
cloud_species = []

#######################################################
# Define retrieval model
#######################################################

def retrieval_model(pRT_object, \
                    parameters, \
                    PT_plot_mode = False):

    # Make the P-T profile
    pressures = pRT_object.press/1e6
    temperatures = nc.guillot_global(pressures, \
                                    parameters['kappa_IR'].value, \
                                    parameters['gamma'].value, \
                                    parameters['gravity'].value, \
                                    parameters['Tint'].value, \
                                    parameters['Tequ'].value)
    
    # If in evaluation mode, and PTs are supposed to be plotted
    if PT_plot_mode:
        return pressures, temperatures
    
    # Make the abundance profile
    COs = parameters['C/O'].value * np.ones_like(pressures)
    FeHs = parameters['[Fe/H]'].value * np.ones_like(pressures)
    
    abundances_interp = pm.interpol_abundances(COs, \
                                               FeHs, \
                                               temperatures, \
                                               pressures)

    abundances = {}
    for species in pRT_object.line_species:
        abundances[species] = abundances_interp[ \
                    species.replace('_all_iso', '').replace('C2H2','C2H2,acetylene')]
    abundances['H2'] = abundances_interp['H2']
    abundances['He'] = abundances_interp['He']
    
    MMW = abundances_interp['MMW']

    # Calculate the spectrum
    pRT_object.calc_transm(temperatures, \
                           abundances, \
                           parameters['gravity'].value, \
                           MMW, \
                           R_pl=parameters['R_pl'].value, \
                           P0_bar=100.)
                           # Keep P0_bar at 100. for now!
                           # Otherwise change maximum pressure
                           # value for pressures = np.logspace(-6, 2, 100)
                           # in retrieve.py!

    wlen_model = nc.c/pRT_object.freq/1e-4
    spectrum_model = (pRT_object.transm_rad/parameters['Rstar'].value)**2.
    
    return wlen_model, spectrum_model


#######################################################
# Read in obeservational files to be used in retrieval
#######################################################

# This will contain the data objects.
# The "data" list will be read by retrieve.py
data = []

if run_mode != 'spectrum':
    data.append(Data('JWST NIRISS SOSS', \
                     'observations/NIRISS_SOSS.dat', \
                     model_generating_function = retrieval_model))
    data.append(Data('JWST NIRSpec', \
                     'observations/NIRSpec_G395M.dat', \
                     model_generating_function = retrieval_model))
    data.append(Data('JWST MIRI LRS', \
                     'observations/MIRI_LRS.dat', \
                     model_generating_function = retrieval_model))
else:
    data.append(Data('generated_spectrum_transmission', \
                     generate_spectrum_wlen_range_micron = \
                     generate_spectrum_wlen_range_micron, \
                     model_generating_function = retrieval_model))
    pandexo_name = data[-1].name

##################################################################
# Define what to put into corner plot if run_mode == 'evaluate'
##################################################################

parameters['R_pl'].plot_in_corner = True
parameters['R_pl'].corner_label = r'$R_{\rm P}$ ($\rm R_{Jup}$)'
parameters['R_pl'].corner_transform = lambda x : x/nc.r_jup_mean
parameters['[Fe/H]'].plot_in_corner = True
parameters['[Fe/H]'].corner_ranges = [-1,1]
parameters['C/O'].plot_in_corner = True
parameters['[Fe/H]'].corner_ranges = [0., 1.]

##################################################################
# Define axis properties of spectral plot if run_mode == 'evaluate'
##################################################################

spec_xlabel = 'Wavelength (micron)'
spec_ylabel = r'$(R_{\rm P}/R_*)^2$ (ppm)'
y_axis_scaling = 1e6
xscale = 'log'
yscale = 'linear'
resolution = 50.

##################################################################
# Define from which observation object to take P-T
# in evaluation mode (if run_mode == 'evaluate'),
# add PT-envelope plotting options
##################################################################

take_PTs_from = 'JWST NIRISS SOSS'
temp_limits = [450, 2000]
press_limits = [1e1, 1e-4]
